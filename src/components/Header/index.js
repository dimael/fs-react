import React, {Component} from 'react';
import './style.less';

class Header extends Component {
  render() {
    return (
      <header className="site-header">
        <div className="site-header__top">
          <div className="container">
            <div className="small-logo"></div>
            <div className="auth">
              <a href="" className="login">log in</a>
              <a href="" className="signup">sign up</a>
              <div className="support">support +1 (844) 260-2204</div>
              <div className="locale">united kingdom</div>
            </div>
          </div>
        </div>
        <div className="site-header__bottom">
          <div className="container">
            <a href="#" className="site-logo"></a>
            <ul className="navigation">
              <li><a href="">Features</a></li>
              <li><a href="">Pricing</a></li>
              <li><a href="">Partners</a></li>
              <li><a href="">Support</a></li>
              <li><a href="">Compare</a></li>
              <li><a href="">Contact</a></li>
              <li><a href="">News</a></li>
            </ul>
          </div>
        </div>
      </header>
    );
  }
}

export default Header;