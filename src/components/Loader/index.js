import React, {Component} from 'react';
import './style.less';

class Loader extends Component {
  render() {
    return (
      <div className='loaderWrapper'>
        <div className='loaderPixel'>
          <div></div>
          {Array.from(Array(101), (_, i) => <div className='pixels' key={i}></div>)}
        </div>
      </div>
    );
  }
}

export default Loader;