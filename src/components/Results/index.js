import React, {Component} from 'react';
import './style.less';
import {Progress} from 'antd';
import {v4} from 'uuid';
import {connect} from 'react-redux';
import Single from '../Single';
import 'babel-polyfill';
import {calcScore, renderSingle, toHome} from '../../actions';

class Results extends Component {
  constructor(props) {
    super(props);
    this.state = {render: null};
    this.handleClick = this.handleClick.bind(this);
    this.renderSingle = this.renderSingle.bind(this);
    this.goHome = this.goHome.bind(this);
  }

  componentDidMount() {
    const {app, dispatch} = this.props;
    let counter = 0;
    let finalScore = 0;

    Object.keys(app).map(key => {
      if (app[key].status === ('warning' || 'danger')) {
        counter += 1;
      }

      app[key].table.success.reduce((accumulator, currentValue) => {
        return accumulator + currentValue.value;
      }, 0);
    });

    if (counter === 0) {
      finalScore = 90;
    } else if (0 < counter < 3) {
      finalScore = 75;
    } else {
      finalScore = 30;
    }
    dispatch(calcScore(finalScore));
  }

  handleClick(e, mod) {
    e.preventDefault();
    this.setState({render: mod});
    this.props.dispatch(renderSingle(this.props.app[mod]));
  }

  renderSingle() {
    if (this.state.render) {
      return <Single single={this.props.app[this.state.render]} key={v4()}/>;
    }
  }

  goHome(e) {
    e.preventDefault();
    this.props.dispatch(toHome());
  }

  render() {
    const {single, score, app} = this.props;

    const description = (score === 30) ? (
      <p className="score__description">There is a high risk <span className="f-medium">your site is infected</span>.
        It is also not currently protected.
        You should perform a deeper, more
        continuous scan using <span className="f-bold">cWatch</span>.</p>
    ) : (
      (score === 75) ? (
        <p className="score__description">Your site needs a <span className="f-medium">heath checkup</span>.
          It is also not currently protected.
          You should perform a deeper, more
          continuous scan using <span className="f-bold">cWatch</span>.</p>
      ) : (
        <p className="score__description">Your site does not look <span className="f-medium">compromised</span>.
          However, it is not currently protected.
          You should perform a deeper, more
          continuous scan using <span className="f-bold">cWatch</span>.</p>
      )
    );

    let content = single ? (
      <Single single={single} score={score} key={v4()}/>
    ) : (
      <div className="section results__inner container">
        <div className="results__left">
          <div className="results__header">Overall Score</div>
          <div className="score">
            <div className="score__circle">
              <Progress
                type="circle"
                percent={score}
                width={311}
                strokeWidth={7}
                strokeColor={(score === 30) ? '#ff3a52' : (score === 75) ? '#ff9f12' : '#1aa307'}/>
            </div>
            {description}
            <a href="https://cwatch.comodo.com/new.php" className="score__link">Learn More About Protecting My Site
              »</a>
          </div>
          <div className="results__footer">
            <a href="https://cwatch.comodo.com/onboard-buyer-journey/" className="button fix-button">fix + protect
              now</a>
          </div>
        </div>
        <div className="results__right">
          <div className="results__header">Tests Performed</div>
          <ul className="grid">
            {
              Object.keys(app).map((key, index) => <GridItem
                key={index}
                title={app[key].title}
                mod={app[key].mod}
                found={app[key].found}
                onClick={this.handleClick}/>
              )
            }
          </ul>
          <div className="results__footer">
            <div className="to-back">
              <a href="#"
                 className="to-back__button"
                 onClick={this.goHome}><i className="to-back__icon"></i>Start a new scan</a>
            </div>
          </div>
        </div>
      </div>
    );

    return (
      <main
        className={(score === 30) ? 'results results--danger' : (score === 75) ? 'results results--warning' : 'results results--success'}>
        {content}
      </main>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    app: state.app,
    single: state.single,
    score: state.score
  };
};

export default connect(mapStateToProps)(Results);

const GridItem = props => {
  const {title, mod, found, onClick} = props;
  const isFound = (found && (found.warning || found.danger)) ? true : false;
  const isRisks = isFound ? `(${found.warning + found.danger}) Risks Found` : null;

  return (
    <li className={'grid__item grid__item--' + mod}>
      <div className="grid__title">{title}</div>
      <i className={isFound ? 'grid__icon grid__icon--risk' : 'grid__icon'}></i>
      <div className="grid__found">
        {isRisks}
      </div>
      <a href="" className="grid__link link" onClick={(event) => onClick(event, mod)}>View Test Details»</a>
    </li>
  );
};

// found.warning + found.danger + ' Risks Found'
