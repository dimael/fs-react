import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Button, Modal} from 'antd';
import Tables from '../Tables';
import {modalTrigger} from '../../actions';

class Details extends Component {
  constructor(props) {
    super(props);
    this.state = {visible: this.props.modal};
    this.handleOk = this.handleOk.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
  }

  handleOk(e) {
    console.log(e);
  }

  handleCancel(e) {
    this.props.dispatch(modalTrigger(false));
  }

  componentDidMount() {
    this.state = {
      visible: this.props.modal
    };
  }

  render() {

    return (
      <div>
        <Modal
          visible={this.state.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
          okText="delete this button"
          width="50%"
          centered
        >
          <Tables/>
        </Modal>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    app: state.app,
    url: state.url,
    single: state.single,
    modal: state.modal
  };
}

export default connect(
  mapStateToProps,
)(Details);
