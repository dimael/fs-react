import React, {Component} from 'react';
import {connect} from 'react-redux';
import JSONTree from 'react-json-tree';
import {Table} from 'antd';
import {v4} from 'uuid';
import './style.less';
import {clearSingle, modalTrigger} from '../../actions';

class Tables extends Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(e) {
    e.preventDefault();
    this.props.dispatch(modalTrigger(false));
    this.props.dispatch(clearSingle());
  }

  render() {
    const {title, mod} = this.props.single;
    const {reputation, malware, contentSecurityRisks, cms} = this.props.app;
    const {malicious, suspicious} = malware.popup.dataSource;
    const {ssl, fls, phishing, gsb} = reputation.popup.dataSource;
    const {vulnerabilities} = cms.popup.dataSource;
    const {content_applications, content_iframes, content_links} = contentSecurityRisks.popup.dataSource;

    let reputationDataSource = [],
      maliciousDataSource = [],
      suspiciousDataSource = [],
      contentIframesDataSource = [],
      contentLinksDataSource = [],
      cmsDataSource = [];

    ssl && ssl.map(item => {
      reputationDataSource.push({
        key: v4(),
        name: item[0],
        type: <a href={'https://sslanalyzer.comodoca.com/?url=' + this.props.url} target="_blank">Click for details</a>
      });
    });
    fls && fls.map(item => {
      reputationDataSource.push({
        key: v4(),
        name: item[0],
        type: item[1]
      });
    });
    phishing && phishing.map(item => {
      reputationDataSource.push({
        key: v4(),
        name: item[0],
        type: item[1]
      });
    });
    gsb && gsb.map(item => {
      reputationDataSource.push({
        key: v4(),
        name: item[0],
        type: item[1]
      });
    });

    malicious && malicious.map(item => {
      maliciousDataSource.push({
        key: v4(),
        sig_id: item[0],
        sig_name: item[1],
        group_name: item[2],
        severity: item[3]
      });
    });
    suspicious && suspicious.map(item => {
      suspiciousDataSource.push({
        key: v4(),
        sig_id: item[0],
        sig_name: item[1],
        group_name: item[2],
        severity: item[3]
      });
    });

    content_iframes.malicious && content_iframes.malicious.map(item => {
      contentIframesDataSource.push({
        key: v4(),
        type: 'malicious',
        location: item
      });
    });
    content_iframes.suspicious && content_iframes.suspicious.map(item => {
      contentIframesDataSource.push({
        key: v4(),
        type: 'suspicious',
        location: item
      });
    });

    content_links.iframes && content_links.iframes.internal.map(item => {
      contentLinksDataSource.push({
        key: v4(),
        type: 'internal iframes',
        location: item
      });
    });
    content_links.iframes && content_links.iframes.external.map(item => {
      contentLinksDataSource.push({
        key: v4(),
        type: 'external iframes',
        location: item
      });
    });
    content_links.scripts && content_links.scripts.internal.map(item => {
      contentLinksDataSource.push({
        key: v4(),
        type: 'internal scripts',
        location: item
      });
    });
    content_links.scripts && content_links.scripts.external.map(item => {
      contentLinksDataSource.push({
        key: v4(),
        type: 'external scripts',
        location: item
      });
    });
    content_links.links && content_links.links.internal.map(item => {
      contentLinksDataSource.push({
        key: v4(),
        type: 'internal links',
        location: item
      });
    });
    content_links.links && content_links.links.external.map(item => {
      contentLinksDataSource.push({
        key: v4(),
        type: 'external links',
        location: item
      });
    });

    vulnerabilities && vulnerabilities.map(item => {
      cmsDataSource.push({
        key: v4(),
        application_name: item.title,
        type: item.vuln_type
      });
    });

    const reputationTable = (
      <div>
        <div className="title">
          <span
            className="red">{(reputation.found.warning + reputation.found.danger) + ' reputation risks'}
          </span>
          <span>{' found for' + this.props.url}</span>
        </div>
        <a href="" className="to-results" onClick={this.handleClick}>Return to Overall Score</a>
        {ssl &&
        <Table dataSource={reputationDataSource} columns={reputation.popup.columns}/>}
      </div>
    );

    const malwareTable = (
      <div>
        <div className="title">
          <span
            className="red">{(malware.found.warning + malware.found.danger) + ' malware risks'}
          </span>
          <span>{' found for' + this.props.url}</span>
        </div>
        <a href="" className="to-results" onClick={this.handleClick}>Return to Overall Score</a>
        {malicious &&
        <Table dataSource={maliciousDataSource} columns={malware.popup.columns} title={() => 'Malicious'}/>}
        {suspicious &&
        <Table dataSource={suspiciousDataSource} columns={malware.popup.columns} title={() => 'Suspicious'}/>}
      </div>
    );

    const contentSecurityRisksTable = (
      <div>
        <div className="title">
          <span
            className="red">
            {
              (contentSecurityRisks.found.warning + contentSecurityRisks.found.danger) + ' content security risks'
            }
          </span>
          <span>{' found for' + this.props.url}</span>
        </div>
        <a href="" className="to-results" onClick={this.handleClick}>Return to Overall Score</a>
        {
          contentIframesDataSource && <Table
            dataSource={contentIframesDataSource}
            columns={contentSecurityRisks.popup.columns}
            title={() => 'Content Iframes'}/>
        }
        {
          contentLinksDataSource && <Table
            dataSource={contentLinksDataSource}
            columns={contentSecurityRisks.popup.columns}
            title={() => 'Content Links'}/>
        }
      </div>
    );
    const cmsTable = (
      <div>
        <div className="title">
          <span
            className="red">
            {
              (cms.found.warning + cms.found.danger) + ' cms'
            }
          </span>
          <span>{' found for' + this.props.url}</span>
        </div>
        <a href="" className="to-results" onClick={this.handleClick}>Return to Overall Score</a>
        {
          cmsDataSource && <Table
            dataSource={cmsDataSource}
            columns={cms.popup.columns}
            />
        }
      </div>
    );

    return (
      <div>
        {mod === 'reputation' ? reputationTable : null}
        {mod === 'malware' ? malwareTable : null}
        {mod === 'contentSecurityRisks' ? contentSecurityRisksTable : null}
        {mod === 'cms' ? cmsTable : null}
        {/*<JSONTree data={this.props} invertTheme={false}/>*/}
      </div>

    );
  }
}

function mapStateToProps(state) {
  return {
    app: state.app,
    url: state.url,
    single: state.single
  };
}

export default connect(
  mapStateToProps
)(Tables);
