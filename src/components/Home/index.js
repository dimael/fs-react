import React, {Component} from 'react';
import './style.less';
import {connect} from 'react-redux';
import {loaderTrigger, setUrl, starter} from '../../actions';
import {url} from '../../settings';
import Recaptcha from 'react-recaptcha';
import {message} from 'antd';

let recaptchaInstance;

const isValid = string => {
  let isSymbol = /[\s!@#$%^&*(),+=?":;№{}|<>]/.test(string);
  let isDomain = string && string.indexOf('.') < 0 ? false : true;

  if (! isSymbol && isDomain) {
    return true;
  }
  return false;
};

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {value: ''};

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.verifyCallback = this.verifyCallback.bind(this);
  }

  componentDidMount() {
    const params = new URLSearchParams(document.location.search.substring(1));
    const query = params.get('q');

    if (params && query) {
      this.props.dispatch(starter(query));
      this.props.dispatch(setUrl(query));
      // grecaptcha.execute();
    } else {
      return null;
    }
  }

  handleChange(event) {
    this.setState({value: event.target.value});
  }

  handleSubmit(event) {
    event.preventDefault();
    if (! this.state.value || ! isValid(this.state.value)) {
      message.error('Enter correct domain');
    } else {
      this.props.dispatch(loaderTrigger(true));
      recaptchaInstance.execute();
    }
  }

  verifyCallback(key) {
    return fetch(`${url}recaptcha`, {
      method: 'POST',
      headers: {
        Accept: 'application/vnd.cww_light.v1',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        'g-recaptcha-response': key,
      }),
    })
      .then(res => {
        return res.json();
      })
      .then(json => {
        recaptchaInstance.reset();
        if (json.success) {
          console.log('captcha is ok');
          this.props.dispatch(starter(this.state.value.trim().replace(/(^\w+:|^)\/\//, '')));
          this.props.dispatch(starter(this.state.value.trim().replace(/(^\w+:|^)\/\//, '')));
          // this.props.dispatch(starter('cww.lx1-cwatch.xyz'));
          // this.props.dispatch(setUrl('cww.lx1-cwatch.xyz'));
        }
      })
      .catch(err => {
        recaptchaInstance.reset();
        this.props.dispatch(loaderTrigger(false));
        message.error(err);
        console.log(err);
      });
  };

  render() {
    return (
      <main className="home">
        <section className="home__inner container">
          <h1 className="home__title">Free Online Website <span className="c-red">Security</span> Scan</h1>
          <div className="home__title-add">Scan any website for <span className="c-red"> malware</span><i></i></div>
          <form className="home-form" id="home-form" onSubmit={this.handleSubmit}>
            <i className="home-form__icon"></i>
            <input type="text"
                   id="inputUrl"
                   className="home-form__input"
                   placeholder="Scan any website"
                   value={this.state.value}
                   onChange={this.handleChange}/>
            <input
              type="submit"
              className="home-form__button g-recaptcha"
              id="scan-button"
              value="scan"/>
            <Recaptcha
              ref={e => recaptchaInstance = e}
              sitekey="6LeDfWMUAAAAAF0RUUIc2TMeEV5MhGpFIw-i4org"
              size="invisible"
              verifyCallback={this.verifyCallback}
            />
          </form>
        </section>
      </main>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    app: state.app,
  };
};

export default connect(mapStateToProps)(Home);
