import React, {Component} from 'react';
import Header from '../Header';
import Footer from '../Footer';
import Home from '../Home';
import './style.less';
import Loader from '../Loader';
import connect from 'react-redux/es/connect/connect';
import {HOME, RESULTS} from '../../settings';
import Results from '../Results';

class App extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {page, loader} = this.props;
    const content = (
      page === HOME ? <Home/> : (
        page === RESULTS ? <Results/> : null
      )
    );
    return (
      <div className="layout">
        <Header/>
        {content}
        {loader ? <Loader/> : null}
        <Footer/>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    page: state.page,
    loader: state.loader
  };
};

export default connect(mapStateToProps)(App);



