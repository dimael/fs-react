import React, {Component} from 'react';
import './style.less';

class Footer extends Component {
  render() {
    return (
      <footer className="site-footer">
        <div className="container">
          <p className="copyright">© 2018 Comodo Security Solutions, Inc. All rights reserved. All trademarks displayed
            on this web site are the exclusive property of the respective holders.</p>
        </div>
      </footer>
    );
  }
}

export default Footer;