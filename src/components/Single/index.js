import React, {Component} from 'react';
import './style.less';
import {v4} from 'uuid';
import JSONTree from 'react-json-tree';
import connect from 'react-redux/es/connect/connect';
import {clearSingle, modalTrigger} from '../../actions';
import Details from '../Details';
import {DANGER, SUCCESS, WARNING} from '../../settings';

class Single extends Component {
  constructor(props) {
    super(props);
    this.goResults = this.goResults.bind(this);
    this.handleModal = this.handleModal.bind(this);
  }

  goResults(e) {
    e.preventDefault();
    this.props.dispatch(modalTrigger(false));
    this.props.dispatch(clearSingle());
  }

  handleModal() {
    this.props.dispatch(modalTrigger(true));
  }

  render() {
    const {title, description, table, found, status, mod} = this.props.single;
    const {modal} = this.props;
    let nomodal = false;
    switch (mod) {
      case 'webAppRisks':
      case 'contentSecurityRisks':
      case 'http':
        nomodal = true;
        break;
      default:
        nomodal = false;
    }
    return (
      <div className="single">
        <div className="container">
          <div className="single__head">
            <i className="single__icon"></i>
            <div className="single__title">{title}</div>
            <div className="single__subtitle">Test Results</div>
            <div className="single__description">{description}</div>
            <a href="" className="back-to" onClick={this.goResults}><i></i>Return to Overall Score</a>
          </div>
          <div className="single__body">
            <ul className="levels">
              <li className="levels__item levels__item--success">
                <div className="levels__head">
                  <div className="levels__badge">{found.success}</div>
                  <div className="levels__text">Level <span className="value">1</span> Risks found</div>
                </div>
                <ul className="levels-table">
                  <li>
                    <div className="levels-table__key">Level 1 Risks</div>
                    <div className="levels-table__value">
                      {
                        table.success.reduce((accumulator, currentValue) => {
                          return accumulator + currentValue.value;
                        }, 0) + ' found'
                      }
                    </div>
                  </li>
                  {
                    table.success.map(item => {
                      return (
                        <li key={v4()}>
                          <div className="levels-table__key">{item.name}</div>
                          <div className="levels-table__value">{item.value}</div>
                        </li>
                      );
                    })
                  }
                </ul>
              </li>
              <li className={'levels__item levels__item--warning' + ' ' + (status === SUCCESS ? 'levels__item--nomodal' : (nomodal ? 'levels__item--nomodal' : ''))}>
                <div className="levels__head">
                  <div className="levels__badge">{found.warning}</div>
                  <div className="levels__text">Level <span className="value">2</span> Risks found</div>
                </div>
                <ul className="levels-table">
                  <li onClick={((status === (DANGER || WARNING)) && !nomodal) ? this.handleModal : null}>
                    <div className="levels-table__key">Level 2 Risks</div>
                    <div className="levels-table__value">
                      {
                        table.warning.reduce((accumulator, currentValue) => {
                          return accumulator + currentValue.value;
                        }, 0) + ' found'
                      }
                    </div>
                  </li>
                  {
                    table.warning.map(item => {
                      return (
                        <li key={v4()}>
                          <div className="levels-table__key">{item.name}</div>
                          <div className="levels-table__value">{item.value}</div>
                        </li>
                      );
                    })
                  }
                </ul>
              </li>
              <li className={'levels__item levels__item--danger' + ' ' + (status === SUCCESS ? 'levels__item--nomodal' : (nomodal ? 'levels__item--nomodal' : ''))}>
                <div className="levels__head">
                  <div className="levels__badge">{found.danger}</div>
                  <div className="levels__text">Level <span className="value">3</span> Risks found</div>
                </div>
                <ul className="levels-table">
                  <li onClick={((status === (DANGER || WARNING)) && !nomodal) ? this.handleModal : null}>
                    <div className="levels-table__key">Level 3 Risks</div>
                    <div className="levels-table__value">
                      {
                        table.danger.reduce((accumulator, currentValue) => {
                          return accumulator + currentValue.value;
                        }, 0) + ' found'
                      }
                    </div>
                  </li>
                  {
                    table.danger.map(item => {
                      return (
                        <li key={v4()}>
                          <div className="levels-table__key">{item.name}</div>
                          <div className="levels-table__value">{item.value}</div>
                        </li>
                      );
                    })
                  }
                </ul>
              </li>
            </ul>
          </div>
          <div className="single__footer">
            <a href="#" className="single__protect-button">protect now</a>
            <a href="#" className="single__fix-button">fix for free</a>
          </div>
        </div>
        {modal ? <Details/> : null}
      </div>
    );
  }

};

const mapStateToProps = (state) => {
  return {
    app: state.app,
    single: state.single,
    score: state.score,
    found: state.found,
    modal: state.modal
  };
};

export default connect(mapStateToProps)(Single);
