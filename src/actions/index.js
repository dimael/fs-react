import {
  url,
  reputationApi,
  TIMEOUT,
  lightweightsApi,
  cmsApi,
  API_REPUTATION,
  API_LIGHTWEIGHTS,
  FETCH_REQUEST,
  FETCH_COMPLETE,
  FETCH_FAIL,
  API_CMS,
  CMS_STATUS,
  REPUTATION_STATUS,
  DANGER,
  SUCCESS,
  WARNING,
  REPUTATION_TABLE,
  CMS_TABLE,
  MALWARE_TABLE,
  MALWARE_STATUS,
  WEBAPPRISKS_TABLE,
  WEBAPPRISKS_STATUS,
  CONTENTSECURITYRISKS_STATUS,
  CONTENTSECURITYRISKS_TABLE,
  HTTP_TABLE,
  HTTP_STATUS,
  HOME,
  RESULTS,
  CLEAR,
  RENDER,
  SCORE,
  SET_DESCRIPTION,
  REPUTATION_FOUND,
  CMS_FOUND,
  CONTENTSECURITYRISKS_FOUND,
  WEBAPPRISKS_FOUND,
  MALWARE_FOUND,
  HTTP_FOUND,
  SET_URL, MALWARE_POPUP, REPUTATION_POPUP, CMS_POPUP, HTTP_POPUP, CONTENTSECURITYRISKS_POPUP, WEBAPPRISKS_POPUP,
  MODAL_TRIGGER, LOADER_TRIGGER
} from '../settings';

const sleep = time => new Promise(resolve => setTimeout(resolve, time));
const fetchJson = (url, params) => fetch(url, params)
  .then(r => r.json())
  .then(data => data)
  .catch(error => console.log(error));

const fetchResult = async (url, data, timeout = TIMEOUT, interval = 10000) => {
  if (data.error) {
    throw new Error(data.message);
  }

  const _url = `${url}/${data.url}`;
  const untilTime = Date.now() + timeout;
  let result = await fetchJson(_url);

  while (result.in_progress && Date.now() < untilTime) {
    console.log(result);
    await sleep(interval);
    result = await fetchJson(_url);
  }

  if (result.in_progress) {
    throw new Error('Timeout');
  }

  return result;
};

const request = async (url, api, testUrl) => {
  const params = {
    method: 'post',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      'url': testUrl
    }),
  };

  const data = await fetchJson(url + api, params);

  return fetchResult(`${url}${api}`, data);
};

const tester = (target, dispatch) => {
  const checkReputations = request(url, reputationApi, target)
    .then((json) => {
      dispatch(apiReputation(json));
      analyzer(json, dispatch);
    })
    .catch(console.error);

  const checkLightweightsApi = request(url, lightweightsApi, target)
    .then((json) => {
      dispatch(apiLightweights(json));
      analyzer(json, dispatch);
    })
    .catch(console.error);

  const checkCmsApi = request(url, cmsApi, target)
    .then((json) => {
      dispatch(apiCms(json));
      analyzer(json, dispatch);
    })
    .catch(console.error);

  return Promise.all([checkReputations, checkLightweightsApi, checkCmsApi]);
};

export const starter = (target) => {
  return (dispatch, getState) => {
    dispatch(fetchRequest());
    dispatch(loaderTrigger(true));
    tester(target, dispatch)
      .then(() => {
        dispatch(fetchComplete());
        dispatch(loaderTrigger(false));
        dispatch(toResults());
        dispatch(reputationFound(getState));
        dispatch(malwareFound(getState));
        dispatch(webApplicationRisksFound(getState));
        dispatch(contentSecurityRisksFound(getState));
        dispatch(httpFound(getState));
        dispatch(cmsFound(getState));
      });
  };
};

export const analyzer = (json, dispatch) => {
  const {ssl, phishtank, fls, gsb} = json.data ? json.data : {};
  const cms = json.status && json;
  const {cdn, content_applications, content_iframes, content_links, content_sig_scan, http_code, http_headers, http_security_headers, waf} = json;

  if (ssl || phishtank || fls || gsb) {
    reputationAnalyzer(dispatch, ssl, phishtank, fls, gsb, url);
  }
  if (content_sig_scan) {
    malwareAnalyzer(dispatch, content_sig_scan);
  }
  if (cdn || waf) {
    webApplicationSecurityRisksAnalyzer(dispatch, cdn, waf);
  }
  if (content_applications || content_iframes || content_links) {
    contentSecurityRisksAnalyzer(dispatch, content_applications, content_iframes, content_links);
  }
  if (http_code || http_headers || http_security_headers) {
    httpSecurityIssuesAnalyzer(dispatch, http_code, http_headers, http_security_headers);
  }
  if (cms) {
    cmsAnalyzer(dispatch, cms);
  }
};

const reputationAnalyzer = (dispatch, ssl, phishtank, fls, gsb, url) => {

  let sslStatus,
    phishtankStatus,
    flsStatus,
    gsbStatus,
    status,
    popup = {
      ssl: [],
      fls: [],
      phishing: [],
      gsb: []
    },
    table = {
      success: [
        {
          name: 'SSL certificate is valid',
          value: 0,
        },
        {
          name: 'No SSL certificate errors detected',
          value: 0,
        },
        {
          name: 'Website is not marked as suspicious by Comodo',
          value: 0,
        },
        {
          name: 'Website is not a phishing',
          value: 0,
        },
        {
          name: 'Website is not blocked by Google Safe Browsing',
          value: 0,
        },
      ],
      warning: [
        {
          name: 'SSL certificate contains errors or is not correctly configured',
          value: 0,
        },
        {
          name: 'SSL certificate errors detected',
          value: 0,
        },
        {
          name: 'Website is marked as suspicious by Comodo',
          value: 0,
        },
      ],
      danger: [
        {
          name: 'SSL certificate is invalid',
          value: 0,
        },
        {
          name: 'Critical SSL certificate errors detected',
          value: 0,
        },
        {
          name: 'Website is marked as malicious by Comodo',
          value: 0,
        },
        {
          name: 'Website marked as a phishing',
          value: 0,
        },
        {
          name: 'Website is blocked by Google Safe Browsing',
          value: 0,
        },
      ],
    };

  if ((ssl.data.certificate && ssl.data.certificate.trusted === 0) || ! ssl.data) {
    sslStatus = DANGER;
    popup.ssl.push(['SSL issue', 'link']);
  }
  else {
    sslStatus = SUCCESS;
  }

  if (fls.data.category <= 2) {
    flsStatus = SUCCESS;
  } else if (4 <= fls.data.category && fls.data.category <= 7) {
    flsStatus = DANGER;
    popup.fls.push(['FLS', 'Malicious']);
  } else {
    flsStatus = WARNING;
    popup.fls.push(['FLS', 'Suspicious']);
  }

  if (phishtank.data.phishing) {
    phishtankStatus = DANGER;
    popup.phishing.push(['Phishtank', 'Phishing']);
  } else {
    phishtankStatus = SUCCESS;
  }

  if (gsb && gsb.data.verdict.length > 0) {
    gsbStatus = DANGER;
    let verdict = [];
    gsb.data.verdict.map(item => {
      verdict.push(item);
    });
    popup.gsb.push(verdict);
  } else {
    gsbStatus = SUCCESS;
  }

  sslStatus === SUCCESS && (table.success[0].value += 1);
  sslStatus === SUCCESS && (table.success[1].value += 1);
  flsStatus === SUCCESS && (table.success[2].value += 1);
  phishtankStatus === SUCCESS && (table.success[3].value = 1);
  gsbStatus === SUCCESS && (table.success[4].value += 1);
  sslStatus === WARNING && (table.warning[0].value += 1);
  sslStatus === WARNING && (table.warning[1].value += 1);
  flsStatus === WARNING && (table.warning[2].value += 1);
  sslStatus === DANGER && (table.danger[0].value += 1);
  sslStatus === DANGER && (table.danger[1].value += 1);
  flsStatus === DANGER && (table.danger[2].value += 1);
  phishtankStatus === DANGER && (table.danger[3].value += 1);
  gsbStatus === DANGER && (table.danger[4].value += 1);

  if (phishtankStatus === DANGER || gsbStatus === DANGER || flsStatus === DANGER) {
    status = DANGER;
  } else if (sslStatus === DANGER || flsStatus === WARNING) {
    status = DANGER;
  } else if (sslStatus === WARNING) {
    status = WARNING;
  } else {
    status = SUCCESS;
  }

  dispatch(reputationStatus(status));
  dispatch(reputationTable(table));
  dispatch(reputationPopup(popup));

};
const malwareAnalyzer = (dispatch, content_sig_scan) => {
  let status,
    table = {
      success: [
        {
          name: 'Suspicious code is not detected',
          value: 0,
        },
        {
          name: 'Defacement is not detected',
          value: 0,
        },
      ],
      warning: [
        {
          name: 'Suspicious code is detected',
          value: 0,
        },
      ],
      danger: [
        {
          name: 'Malicious code is detected',
          value: 0,
        },
        {
          name: 'Defacement is detected',
          value: 0,
        },
      ],
    },
    popup = {};

  if (content_sig_scan.malicious && content_sig_scan.malicious.length !== 0) {
    status = DANGER;
    table.danger[0].value += content_sig_scan.malicious.length;
    popup.malicious = content_sig_scan.malicious;
  } else if (content_sig_scan.suspicious && content_sig_scan.suspicious.length !== 0) {
    status = WARNING;
    table.warning[0].value += content_sig_scan.suspicious.length;
    popup.suspicious = content_sig_scan.suspicious;
  } else {
    status = SUCCESS;
    table.success[0].value += 1;
    table.success[1].value += 1;
  }

  dispatch(malwareStatus(status));
  dispatch(malwareTable(table));
  dispatch(malwarePopup(popup));
};
const webApplicationSecurityRisksAnalyzer = (dispatch, cdn, waf) => {
  let status,
    cdnStatus,
    wafStatus,
    table = {
      success: [
        {
          name: 'Site is under Comodo Content Delivery Network',
          value: 0,
        },
        {
          name: 'Site is protected by Comodo Web Application Firewall',
          value: 0,
        },
      ],
      warning: [
        {
          name: 'Site is under unknown Content Delivery Network',
          value: 0,
        },
        {
          name: 'Site is protected by unknown Web Application Firewall',
          value: 0,
        },
      ],
      danger: [
        {
          name: 'Site is not under Content Delivery Network',
          value: 0,
        },
        {
          name: 'Site is not protected by Web Application Firewall',
          value: 0,
        },
      ],
    };

  if (cdn.comodo && cdn.name) {
    cdnStatus = SUCCESS;
  } else if ((! cdn.comodo && cdn.name) || (cdn.comodo && ! cdn.name)) {
    cdnStatus = WARNING;
  } else {
    cdnStatus = DANGER;
  }

  if (waf.comodo && waf.name) {
    wafStatus = SUCCESS;
  } else if ((! waf.comodo && waf.name) || (waf.comodo && ! waf.name)) {
    wafStatus = WARNING;
  } else {
    wafStatus = DANGER;
  }

  if (cdnStatus === SUCCESS && wafStatus === SUCCESS) {
    status = SUCCESS;
    table.success[0].value = 1;
    table.success[1].value = 1;

  } else if (cdnStatus === SUCCESS && wafStatus === WARNING) {
    status = WARNING;
    table.success[0].value = 1;
    table.warning[1].value = 1;

  } else if (cdnStatus === WARNING && wafStatus === SUCCESS) {
    status = WARNING;
    table.warning[0].value = 1;
    table.success[1].value = 1;

  } else if (cdnStatus === WARNING && wafStatus === WARNING) {
    status = WARNING;
    table.warning[0].value = 1;
    table.warning[1].value = 1;

  } else if (cdnStatus === SUCCESS && wafStatus === WARNING) {
    status = WARNING;
    table.success[0].value = 1;
    table.warning[1].value = 1;

  } else if (cdnStatus === SUCCESS && wafStatus === DANGER) {
    status = DANGER;
    table.success[0].value = 1;
    table.danger[1].value = 1;

  } else if (cdnStatus === DANGER && wafStatus === SUCCESS) {
    status = WARNING;
    table.danger[0].value = 1;
    table.success[1].value = 1;

  } else if (cdnStatus === DANGER && wafStatus === WARNING) {
    status = WARNING;
    table.danger[0].value = 1;
    table.warning[1].value = 1;

  } else if (cdnStatus === WARNING && wafStatus === DANGER) {
    status = DANGER;
    table.warning[0].value = 1;
    table.danger[1].value = 1;

  } else if (cdnStatus === DANGER && wafStatus === DANGER) {
    status = DANGER;
    table.danger[0].value = 1;
    table.danger[1].value = 1;
  }

  dispatch(webApplicationRisksStatus(status));
  dispatch(webApplicationRisksTable(table));

};
const contentSecurityRisksAnalyzer = (dispatch, content_applications, content_iframes, content_links) => {
  let status,
    popup = {
      content_iframes: {},
      content_links: {}
    },
    table = {
      success: [
        {
          name: 'Suspicious content iframes are not detected',
          value: 0,
        },
        {
          name: 'Suspicious web applications are not detected',
          value: 0,
        },
        {
          name: 'Suspicious content links are not detected',
          value: 0,
        },
      ],
      warning: [
        {
          name: 'Suspicious content iframes are detected',
          value: 0,
        },
        {
          name: 'Suspicious web applications are detected',
          value: 0,
        },
        {
          name: 'Suspicious content links are detected',
          value: 0,
        },
      ],
      danger: [
        {
          name: 'Malicious content iframes are detected',
          value: 0,
        },
        {
          name: 'Malicious web applications are detected',
          value: 0,
        },
        {
          name: 'Malicious content links are detected',
          value: 0,
        },
      ],
    },
    contentIframesStatus,
    contentLinksStatus,
    contentApplicationsStatus = SUCCESS;

  if (content_iframes.malicious.length !== 0) {
    contentIframesStatus = DANGER;
  } else if (content_iframes.suspicious.length !== 0) {
    contentIframesStatus = WARNING;
  } else {
    contentIframesStatus = SUCCESS;
  }

  if ((content_links.iframes && content_links.iframes.external.length !== 0) && (content_links.iframes && content_links.iframes.internal.length !== 0)) {
    contentLinksStatus = DANGER;
  } else {
    contentLinksStatus = SUCCESS;
  }

  contentIframesStatus === SUCCESS && (table.success[0].value += 1);
  contentApplicationsStatus === SUCCESS && (table.success[1].value += 1);
  contentLinksStatus === SUCCESS && (table.success[2].value += 1);

  if (contentApplicationsStatus === SUCCESS && contentIframesStatus === SUCCESS && contentLinksStatus === SUCCESS) {
    status = SUCCESS;
  }

  if (contentIframesStatus === WARNING) {
    status = WARNING;
    table.warning[0].value += content_iframes.suspicious.length;
  }

  if (contentIframesStatus === DANGER) {
    status = DANGER;
    table.danger[0].value += content_iframes.malicious.length;
  }

  popup.content_iframes = content_iframes;
  popup.content_links = content_links;

  dispatch(contentSecurityRisksStatus(status));
  dispatch(contentSecurityRisksTable(table));
  dispatch(contentSecurityRisksPopup(popup));

};
const httpSecurityIssuesAnalyzer = (dispatch, http_code, http_headers, http_security_headers) => {
  let status,
    table = {
      success: [
        {
          name: 'Suspicious HTTP codes are not detected',
          value: 0,
        },
        {
          name: 'Suspicious HTTP headers are not detected',
          value: 0,
        },
        {
          name: 'Suspicious HTTP security headers are not detected',
          value: 0,
        },
      ],
      warning: [
        {
          name: 'Suspicious HTTP codes are detected',
          value: 0,
        },
        {
          name: 'Suspicious HTTP headers are detected',
          value: 0,
        },
        {
          name: 'Suspicious HTTP security headers are detected',
          value: 0,
        },
      ],
      danger: [
        {
          name: 'Malicious HTTP codes are detected',
          value: 0,
        },
        {
          name: 'Malicious HTTP headers are detected',
          value: 0,
        },
        {
          name: 'Malicious HTTP security headers are detected',
          value: 0,
        },
      ],
    },
    httpCodeStatus = SUCCESS,
    httpHeadersStatus = SUCCESS,
    httpSecurityHeadersStatus = SUCCESS;

  if (httpCodeStatus === SUCCESS && httpHeadersStatus === SUCCESS && httpSecurityHeadersStatus === SUCCESS) {
    status = SUCCESS;
    table.success[0].value = 1;
    table.success[1].value = 1;
    table.success[2].value = 1;
  }

  dispatch(httpStatus(status));
  dispatch(httpTable(table));
};
const cmsAnalyzer = (dispatch, cms) => {
  const applicationName = cms.application_name ? cms.application_name : 'application';
  let status,
    popup = {
      vulnerabilities: []
    },
    table = {
      success: [
        {
          name: `{applicationName} vulnerabilities are not detected`,
          value: 0,
        },
      ],
      warning: [
        {
          name: `Medium {applicationName} vulnerabilities are detected`,
          value: 0,
        },
      ],
      danger: [
        {
          name: `Critical {applicationName} vulnerabilities are detected`,
          value: 0,
        },
      ]
    };

  if (cms.vulnerable) {
    status = DANGER;
    popup.vulnerabilities = cms.report_list;
  } else {
    status = SUCCESS;
  }

  status === SUCCESS && (table.success[0].value += 1);

  dispatch(cmsStatus(status));
  dispatch(cmsTable(table));
  dispatch(cmsPopup(popup));
};

export const apiReputation = (data) => {
  return {
    type: API_REPUTATION,
    payload: data
  };
};
export const apiLightweights = (data) => {
  return {
    type: API_LIGHTWEIGHTS,
    payload: data
  };
};
export const apiCms = (data) => {
  return {
    type: API_CMS,
    payload: data
  };
};

export const fetchRequest = () => {
  return {
    type: FETCH_REQUEST,
    payload: true
  };
};
export const fetchComplete = () => {
  return {
    type: FETCH_COMPLETE,
    payload: false
  };
};
export const fetchFail = () => {
  return {type: FETCH_FAIL};
};

export const reputationStatus = status => {
  return {
    type: REPUTATION_STATUS,
    payload: status
  };
};
export const reputationTable = table => {
  return {
    type: REPUTATION_TABLE,
    payload: table
  };
};
export const reputationFound = getState => {
  const table = getState().app.reputation.table;
  const initialValue = 0;
  const reducer = (accumulator, currentValue) => {
    return accumulator + currentValue.value;
  };
  let found = {};

  Object.keys(table).map(key => {
    found[key] = table[key].reduce(reducer, initialValue);
  });

  return {
    type: REPUTATION_FOUND,
    payload: found
  };
};
export const reputationPopup = popup => {
  return {
    type: REPUTATION_POPUP,
    payload: popup
  };
};

export const malwareStatus = status => {
  return {
    type: MALWARE_STATUS,
    payload: status
  };
};
export const malwareTable = table => {
  return {
    type: MALWARE_TABLE,
    payload: table
  };
};
export const malwareFound = getState => {
  const table = getState().app.malware.table;
  const initialValue = 0;
  const reducer = (accumulator, currentValue) => {
    return accumulator + currentValue.value;
  };
  let found = {};

  Object.keys(table).map(key => {
    found[key] = table[key].reduce(reducer, initialValue);
  });

  return {
    type: MALWARE_FOUND,
    payload: found
  };
};
export const malwarePopup = popup => {
  return {
    type: MALWARE_POPUP,
    payload: popup
  };
};

export const webApplicationRisksStatus = status => {
  return {
    type: WEBAPPRISKS_STATUS,
    payload: status
  };
};
export const webApplicationRisksTable = table => {
  return {
    type: WEBAPPRISKS_TABLE,
    payload: table
  };
};
export const webApplicationRisksFound = getState => {
  const table = getState().app.webAppRisks.table;
  const initialValue = 0;
  const reducer = (accumulator, currentValue) => {
    return accumulator + currentValue.value;
  };
  let found = {};

  Object.keys(table).map(key => {
    found[key] = table[key].reduce(reducer, initialValue);
  });

  return {
    type: WEBAPPRISKS_FOUND,
    payload: found
  };
};
export const webApplicationRisksPopup = popup => {
  return {
    type: WEBAPPRISKS_POPUP,
    payload: popup
  };
};

export const contentSecurityRisksStatus = status => {
  return {
    type: CONTENTSECURITYRISKS_STATUS,
    payload: status
  };
};
export const contentSecurityRisksTable = table => {
  return {
    type: CONTENTSECURITYRISKS_TABLE,
    payload: table
  };
};
export const contentSecurityRisksFound = getState => {
  const table = getState().app.contentSecurityRisks.table;
  const initialValue = 0;
  const reducer = (accumulator, currentValue) => {
    return accumulator + currentValue.value;
  };
  let found = {};

  Object.keys(table).map(key => {
    found[key] = table[key].reduce(reducer, initialValue);
  });

  return {
    type: CONTENTSECURITYRISKS_FOUND,
    payload: found
  };
};
export const contentSecurityRisksPopup = popup => {
  return {
    type: CONTENTSECURITYRISKS_POPUP,
    payload: popup
  };
};

export const httpStatus = status => {
  return {
    type: HTTP_STATUS,
    payload: status
  };
};
export const httpTable = table => {
  return {
    type: HTTP_TABLE,
    payload: table
  };
};
export const httpFound = getState => {
  const table = getState().app.http.table;
  const initialValue = 0;
  const reducer = (accumulator, currentValue) => {
    return accumulator + currentValue.value;
  };
  let found = {};

  Object.keys(table).map(key => {
    found[key] = table[key].reduce(reducer, initialValue);
  });

  return {
    type: HTTP_FOUND,
    payload: found
  };
};
export const httpPopup = popup => {
  return {
    type: HTTP_POPUP,
    payload: popup
  };
};

export const cmsStatus = status => {
  return {
    type: CMS_STATUS,
    payload: status
  };
};
export const cmsTable = table => {
  return {
    type: CMS_TABLE,
    payload: table
  };
};
export const cmsFound = getState => {
  const table = getState().app.cms.table;
  const initialValue = 0;
  const reducer = (accumulator, currentValue) => {
    return accumulator + currentValue.value;
  };
  let found = {};

  Object.keys(table).map(key => {
    found[key] = table[key].reduce(reducer, initialValue);
  });

  return {
    type: CMS_FOUND,
    payload: found
  };
};
export const cmsPopup = popup => {
  return {
    type: CMS_POPUP,
    payload: popup
  };
};

export const toHome = () => {
  return {
    type: HOME,
    payload: HOME
  };
};
export const toResults = () => {
  return {
    type: RESULTS,
    payload: RESULTS
  };
};
export const modalTrigger = boolean => {
  return {
    type: MODAL_TRIGGER,
    payload: boolean
  };
};
export const renderSingle = single => {
  return {
    type: RENDER,
    payload: single
  };
};
export const clearSingle = single => {
  return {
    type: CLEAR,
    payload: single
  };
};
export const calcScore = score => {
  return {
    type: SCORE,
    payload: score
  };
};
export const setUrl = url => {
  return {
    type: SET_URL,
    payload: url
  };
};
export const loaderTrigger = boolean => {
  return {
    type: LOADER_TRIGGER,
    payload: boolean
  };
};