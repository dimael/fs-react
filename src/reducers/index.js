import {
  API_CMS,
  API_LIGHTWEIGHTS,
  API_REPUTATION,
  APP,
  CMS_STATUS, CONTENTSECURITYRISKS_STATUS,
  HTTP_STATUS, MALWARE_STATUS, REPUTATION_STATUS, WEBAPPRISKS_STATUS,
  FETCH_COMPLETE,
  FETCH_REQUEST, REPUTATION_TABLE, CMS_TABLE, MALWARE_TABLE, WEBAPPRISKS_TABLE, CONTENTSECURITYRISKS_TABLE, HTTP_TABLE,
  SCORE, SINGLE, RESULTS, HOME, RENDER, CLEAR, REPUTATION_FOUND, CMS_FOUND, CONTENTSECURITYRISKS_FOUND,
  WEBAPPRISKS_FOUND, MALWARE_FOUND, HTTP_FOUND, SET_URL, MALWARE_POPUP, REPUTATION_POPUP, CONTENTSECURITYRISKS_POPUP,
  CMS_POPUP, MODAL_TRIGGER, LOADER_TRIGGER
} from '../settings';

const initialState = {
  reputation: {
    title: 'Reputation',
    mod: 'reputation',
    description: 'Website Reputation Issues',
    popup: {
      columns: [
        {
          title: 'name',
          dataIndex: 'name',
          key: 'name'
        },
        {
          title: 'type',
          dataIndex: 'type',
          key: 'type'
        },
      ],
      dataSource: []
    },
    table: {
      success: [
        {
          name: 'SSL certificate is valid',
          value: 0,
        },
        {
          name: 'No SSL certificate errors detected',
          value: 0,
        },
        {
          name: 'Website is not marked as suspicious by Comodo',
          value: 0,
        },
        {
          name: 'Website is not a phishing',
          value: 0,
        },
        {
          name: 'Website is not blocked by Google Safe Browsing',
          value: 0,
        },
      ],
      warning: [
        {
          name: 'SSL certificate contains errors or is not correctly configured',
          value: 0,
        },
        {
          name: 'SSL certificate errors detected',
          value: 0,
        },
        {
          name: 'Website is marked as suspicious by Comodo',
          value: 0,
        },
      ],
      danger: [
        {
          name: 'SSL certificate is invalid',
          value: 0,
        },
        {
          name: 'Critical SSL certificate errors detected',
          value: 0,
        },
        {
          name: 'Website is marked as malicious by Comodo',
          value: 0,
        },
        {
          name: 'Website marked as a phishing',
          value: 0,
        },
        {
          name: 'Website is blocked by Google Safe Browsing',
          value: 0,
        },
      ],
    }
  },
  malware: {
    title: 'Malware',
    mod: 'malware',
    description: 'Malicious and Defacement Risks Found',
    popup: {
      columns: [
        {
          title: 'sig_id',
          dataIndex: 'sig_id',
          key: 'sig_id'
        }, {
          title: 'sig_name',
          dataIndex: 'sig_name',
          key: 'sig_name',
        }, {
          title: 'group_name',
          dataIndex: 'group_name',
          key: 'group_name',
        }, {
          title: 'severity',
          dataIndex: 'severity',
          key: 'severity',
        }
      ],
      dataSource: []
    },
    table: {
      success: [
        {
          name: 'Suspicious code is not detected',
          value: 0,
        },
        {
          name: 'Defacement is not detected',
          value: 0,
        },
      ],
      warning: [
        {
          name: 'Suspicious code is detected',
          value: 0,
        },
      ],
      danger: [
        {
          name: 'Malicious code is detected',
          value: 0,
        },
        {
          name: 'Defacement is detected',
          value: 0,
        },
      ],
    }
  },
  webAppRisks: {
    title: 'Web Application Risks',
    mod: 'webAppRisks',
    description: 'CDN and WAF',
    table: {
      success: [
        {
          name: 'Site is under Comodo Content Delivery Network',
          value: 0,
        },
        {
          name: 'Site is protected by Comodo Web Application Firewall',
          value: 0,
        },
      ],
      warning: [
        {
          name: 'Site is under unknown Content Delivery Network',
          value: 0,
        },
        {
          name: 'Site is protected by unknown Web Application Firewall',
          value: 0,
        },
      ],
      danger: [
        {
          name: 'Site is not under Content Delivery Network',
          value: 0,
        },
        {
          name: 'Site is not protected by Web Application Firewall',
          value: 0,
        },
      ],
    }
  },
  contentSecurityRisks: {
    title: 'Content Security Risks',
    mod: 'contentSecurityRisks',
    description: 'Suspicious Content',
    popup: {
      columns: [
        {
          title: 'type',
          dataIndex: 'type',
          key: 'type',
        },
        {
          title: 'location',
          dataIndex: 'location',
          key: 'location'
        }
      ],
      dataSource: []
    },
    table: {
      success: [
        {
          name: 'Suspicious content iframes are not detected',
          value: 0,
        },
        {
          name: 'Suspicious web applications are not detected',
          value: 0,
        },
        {
          name: 'Suspicious content links are not detected',
          value: 0,
        },
      ],
      warning: [
        {
          name: 'Suspicious content iframes are detected',
          value: 0,
        },
        {
          name: 'Suspicious web applications are detected',
          value: 0,
        },
        {
          name: 'Suspicious content links are detected',
          value: 0,
        },
      ],
      danger: [
        {
          name: 'Malicious content iframes are detected',
          value: 0,
        },
        {
          name: 'Malicious web applications are detected',
          value: 0,
        },
        {
          name: 'Malicious content links are detected',
          value: 0,
        },
      ],
    }
  },
  http: {
    title: 'HTTP Security Issues',
    mod: 'http',
    description: 'HTTP Codes and Headers',
    table: {
      success: [
        {
          name: 'Suspicious HTTP codes are not detected',
          value: 0,
        },
        {
          name: 'Suspicious HTTP headers are not detected',
          value: 0,
        },
        {
          name: 'Suspicious HTTP security headers are not detected',
          value: 0,
        },
      ],
      warning: [
        {
          name: 'Suspicious HTTP codes are detected',
          value: 0,
        },
        {
          name: 'Suspicious HTTP headers are detected',
          value: 0,
        },
        {
          name: 'Suspicious HTTP security headers are detected',
          value: 0,
        },
      ],
      danger: [
        {
          name: 'Malicious HTTP codes are detected',
          value: 0,
        },
        {
          name: 'Malicious HTTP headers are detected',
          value: 0,
        },
        {
          name: 'Malicious HTTP security headers are detected',
          value: 0,
        },
      ],
    }
  },
  cms: {
    title: 'CMS Vulnerabilities',
    mod: 'cms',
    description: 'CMS Vulnerabilities',
    popup: {
      columns: [
        {
          title: 'Application name',
          dataIndex: 'application_name',
          key: 'application_name',
        },
        {
          title: 'type',
          dataIndex: 'type',
          key: 'type'
        }
      ],
      dataSource: []
    },
    table: {
      success: [
        {
          name: `{applicationName} vulnerabilities are not detected`,
          value: 0,
        },
      ],
      warning: [
        {
          name: `Medium {applicationName} vulnerabilities are detected`,
          value: 0,
        },
      ],
      danger: [
        {
          name: `Critical {applicationName} vulnerabilities are detected`,
          value: 0,
        },
      ],
    }
  }
};

export const app = (state = initialState, action) => {
  switch (action.type) {
    case REPUTATION_STATUS:
      return {
        ...state,
        reputation: {
          ...state.reputation,
          status: action.payload
        }
      };
    case REPUTATION_TABLE:
      return {
        ...state,
        reputation: {
          ...state.reputation,
          table: action.payload
        }
      };
    case REPUTATION_FOUND:
      return {
        ...state,
        reputation: {
          ...state.reputation,
          found: action.payload
        }
      };
    case REPUTATION_POPUP:
      return {
        ...state,
        reputation: {
          ...state.reputation,
          popup: {
            ...state.reputation.popup,
            dataSource: action.payload
          }
        }
      };
    case MALWARE_STATUS:
      return {
        ...state,
        malware: {
          ...state.malware,
          status: action.payload
        }
      };
    case MALWARE_TABLE:
      return {
        ...state,
        malware: {
          ...state.malware,
          table: action.payload
        }
      };
    case MALWARE_POPUP:
      return {
        ...state,
        malware: {
          ...state.malware,
          popup: {
            ...state.malware.popup,
            dataSource: action.payload
          }
        }
      };
    case MALWARE_FOUND:
      return {
        ...state,
        malware: {
          ...state.malware,
          found: action.payload
        }
      };
    case WEBAPPRISKS_STATUS:
      return {
        ...state,
        webAppRisks: {
          ...state.webAppRisks,
          status: action.payload
        }
      };
    case WEBAPPRISKS_TABLE:
      return {
        ...state,
        webAppRisks: {
          ...state.webAppRisks
          ,
          table: action.payload
        }
      };
    case WEBAPPRISKS_FOUND:
      return {
        ...state,
        webAppRisks: {
          ...state.webAppRisks,
          found: action.payload
        }
      };
    case CONTENTSECURITYRISKS_STATUS:
      return {
        ...state,
        contentSecurityRisks: {
          ...state.contentSecurityRisks,
          status: action.payload
        }
      };
    case CONTENTSECURITYRISKS_TABLE:
      return {
        ...state,
        contentSecurityRisks: {
          ...state.contentSecurityRisks,
          table: action.payload
        }
      };
    case CONTENTSECURITYRISKS_FOUND:
      return {
        ...state,
        contentSecurityRisks: {
          ...state.contentSecurityRisks,
          found: action.payload
        }
      };
    case CONTENTSECURITYRISKS_POPUP:
      return {
        ...state,
        contentSecurityRisks: {
          ...state.contentSecurityRisks,
          popup: {
            ...state.contentSecurityRisks.popup,
            dataSource: action.payload
          }
        }
      };
    case HTTP_STATUS:
      return {
        ...state,
        http: {
          ...state.http,
          status: action.payload
        }
      };
    case HTTP_TABLE:
      return {
        ...state,
        http: {
          ...state.http,
          table: action.payload
        }
      };
    case HTTP_FOUND:
      return {
        ...state,
        http: {
          ...state.http,
          found: action.payload
        }
      };
    case CMS_STATUS:
      return {
        ...state,
        cms: {
          ...state.cms,
          status: action.payload
        }
      };
    case CMS_TABLE:
      return {
        ...state,
        cms: {
          ...state.cms,
          table: action.payload
        }
      };
    case CMS_FOUND:
      return {
        ...state,
        cms: {
          ...state.cms,
          found: action.payload
        }
      };

    case CMS_POPUP:
      return {
        ...state,
        cms: {
          ...state.cms,
          popup: {
            ...state.cms.popup,
            dataSource: action.payload
          }
        }
      };

    default:
      return state;
  }
};

export const api = (state = {}, action) => {
  switch (action.type) {
    case API_REPUTATION:
      return Object.assign({}, state, {
        reputation: action.payload
      });
    case API_LIGHTWEIGHTS:
      return Object.assign({}, state, {
        lightweights: action.payload
      });
    case API_CMS:
      return Object.assign({}, state, {
        cms: action.payload
      });

    default:
      return state;
  }
};

export const isFetching = (state = false, action) => {
  switch (action.type) {
    case FETCH_REQUEST:
      return true;
    case FETCH_COMPLETE:
      return false;
    default:
      return state;
  }
};

export const score = (state = 0, action) => {
  switch (action.type) {
    case SCORE:
      return action.payload;
    default:
      return state;
  }
};

export const page = (state = HOME, action) => {
  switch (action.type) {
    case HOME:
      return HOME;
    case RESULTS:
      return RESULTS;
    default:
      return state;
  }
};

export const single = (state = null, action) => {
  switch (action.type) {
    case RENDER:
      return action.payload;
    case CLEAR:
      return null;
    default:
      return state;
  }
};

export const modal = (state = false, action) => {
  switch (action.type) {
    case MODAL_TRIGGER:
      return action.payload;
    default:
      return state;
  }
};

export const loader = (state = false, action) => {
  switch (action.type) {
    case LOADER_TRIGGER:
      return action.payload;
    default:
      return state;
  }
};

export const url = (state = '', action) => {
  switch (action.type) {
    case SET_URL:
      return action.payload;
    default:
      return state;
  }
};