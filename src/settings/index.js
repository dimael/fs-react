export const url = 'https://free.stage.cww.comodo.od.ua:8443/';
export const reputationApi = 'reputations';
export const lightweightsApi = 'light_weights';
export const cmsApi = 'cmss';

export const TIMEOUT = 2 * 60 * 1000;

export const jsonTreeTheme = {
  scheme: 'monokai',
  author: 'wimer hazenberg (http://www.monokai.nl)',
  base00: '#272822',
  base01: '#383830',
  base02: '#49483e',
  base03: '#75715e',
  base04: '#a59f85',
  base05: '#f8f8f2',
  base06: '#f5f4f1',
  base07: '#f9f8f5',
  base08: '#f92672',
  base09: '#fd971f',
  base0A: '#f4bf75',
  base0B: '#a6e22e',
  base0C: '#a1efe4',
  base0D: '#66d9ef',
  base0E: '#ae81ff',
  base0F: '#cc6633'
};

export const APP = 'APP';
export const SCORE = 'SCORE';
export const HOME = 'HOME';
export const RESULTS = 'RESULTS';
export const RENDER = 'RENDER';
export const CLEAR = 'CLEAR';
export const MODAL_TRIGGER = 'MODAL_TRIGGER';
export const LOADER_TRIGGER = 'LOADER_TRIGGER';

export const SET_URL = 'SET_URL';

export const REPUTATION_STATUS = 'REPUTATION_STATUS';
export const MALWARE_STATUS = 'MALWARE_STATUS';
export const WEBAPPRISKS_STATUS = 'WEBAPPRISKS_STATUS';
export const CONTENTSECURITYRISKS_STATUS = 'CONTENTSECURITYRISKS_STATUS';
export const HTTP_STATUS = 'HTTP_STATUS';
export const CMS_STATUS = 'CMS_STATUS';

export const REPUTATION_TABLE = 'REPUTATION_TABLE';
export const MALWARE_TABLE = 'MALWARE_TABLE';
export const WEBAPPRISKS_TABLE = 'WEBAPPRISKS_TABLE';
export const CONTENTSECURITYRISKS_TABLE = 'CONTENTSECURITYRISKS_TABLE';
export const HTTP_TABLE = 'HTTP_TABLE';
export const CMS_TABLE = 'CMS_TABLE';

export const REPUTATION_FOUND = 'REPUTATION_FOUND';
export const MALWARE_FOUND = 'MALWARE_FOUND';
export const WEBAPPRISKS_FOUND = 'WEBAPPRISKS_FOUND';
export const CONTENTSECURITYRISKS_FOUND = 'CONTENTSECURITYRISKS_FOUND';
export const HTTP_FOUND = 'HTTP_STATUS_FOUND';
export const CMS_FOUND = 'CMS_FOUND';

export const REPUTATION_POPUP = 'REPUTATION_POPUP';
export const MALWARE_POPUP = 'MALWARE_POPUP';
export const WEBAPPRISKS_POPUP = 'WEBAPPRISKS_POPUP';
export const CONTENTSECURITYRISKS_POPUP = 'CONTENTSECURITYRISKS_POPUP';
export const HTTP_POPUP = 'HTTP_POPUP';
export const CMS_POPUP = 'CMS_POPUP';

export const API_REPUTATION = 'API_REPUTATION';
export const API_LIGHTWEIGHTS = 'API_LIGHTWEIGHTS';
export const API_CMS = 'API_CMS';

export const FETCH_REQUEST = 'FETCH_REQUEST';
export const FETCH_COMPLETE = 'FETCH_COMPLETE';
export const FETCH_FAIL = 'FETCH_FAIL';

export const SUCCESS = 'success';
export const WARNING = 'warning';
export const DANGER = 'danger';